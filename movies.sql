-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 06, 2022 at 08:34 AM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `movies`
--

-- --------------------------------------------------------

--
-- Table structure for table `actor`
--

CREATE TABLE `actor` (
  `act_id` int(11) NOT NULL,
  `act_fname` char(20) NOT NULL,
  `act_lname` char(20) NOT NULL,
  `act_gender` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `actor`
--

INSERT INTO `actor` (`act_id`, `act_fname`, `act_lname`, `act_gender`) VALUES
(1, 'Vin', 'Diesel', 'M'),
(2, 'Robert', 'Downey Jr', 'M'),
(3, 'Dylan', 'O\'Brien', 'M'),
(4, 'Ryan', 'Renolds', 'M'),
(5, 'Andrew', 'Garfield', 'M'),
(6, 'Tobey', 'Maguire', 'M'),
(7, 'Tom', 'Holland', 'M'),
(8, 'Shia', 'LeBeouf', 'M'),
(9, 'Chedwick', 'Boseman', 'M'),
(10, 'Daniel', 'Radcliffe', 'M'),
(11, 'Jesse', 'Elsenberg', 'M'),
(12, 'Brie', 'Larson', 'F'),
(13, 'Dwayne', 'Johnson', 'M'),
(14, 'Will', 'Smith', 'M'),
(15, 'Chris', 'Evans', 'M'),
(16, 'Tom', 'Hanks', 'M'),
(17, 'Chris', 'Pratt', 'M'),
(18, 'Margot', 'Robbie', 'F'),
(19, 'Miles', 'Teller', 'M'),
(20, 'Mads', 'Mikkelsen', 'M'),
(21, 'Samuel L.', 'Jackson', 'M'),
(22, 'Rosa', 'Salazar', 'F'),
(23, 'Simu', 'Liu', 'M'),
(24, 'Benedict', 'Cumberbatch', 'M'),
(25, 'Christian', 'Bale', 'M'),
(26, 'Mayumi', 'Tanaka', 'F'),
(27, 'Haille', 'Steinfeld', 'F'),
(28, 'Teron', 'Egerton', 'M');

-- --------------------------------------------------------

--
-- Table structure for table `director`
--

CREATE TABLE `director` (
  `dir_id` varchar(50) NOT NULL,
  `dir_fname` char(20) NOT NULL,
  `dir_lname` char(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `director`
--

INSERT INTO `director` (`dir_id`, `dir_fname`, `dir_lname`) VALUES
('11', 'Chris', 'Colombus'),
('12', 'Louis', 'Leterrier'),
('13', 'Ryan', 'Fleck'),
('14', 'David', 'Leitch'),
('15', 'Nick', 'Bruno'),
('16', 'Anthony', 'Russo'),
('17', 'Josh', 'Cooley'),
('18', 'Jon', 'Watts'),
('19', 'Dan', 'Scanlon'),
('2', 'Joss', 'Whedon'),
('20', 'Cathy', 'Yan'),
('21', 'Josh', 'Trank'),
('22', 'David', 'Yates'),
('23', 'Brad', 'Bird'),
('24', 'Robert', 'Rodriguez'),
('25', 'Destin', 'Daniel'),
('26', 'Matthew', 'Vaughn'),
('27', 'Sam', 'Raimi'),
('28', 'James', 'Mangold'),
('29', 'Takashi', 'Otsuka'),
('3', 'Wes', 'Ball'),
('30', 'Travis', 'Knight'),
('4', 'Shawn', 'Levy'),
('5', 'Marc', 'Webb'),
('6', 'Sam', 'Raimi'),
('62', 'Janson', 'Button'),
('7', 'Jon', 'Watts'),
('8', 'Michael', 'Bay'),
('9', 'Ryan', 'Coogler');

-- --------------------------------------------------------

--
-- Table structure for table `genres`
--

CREATE TABLE `genres` (
  `gen_id` int(11) NOT NULL,
  `gen_title` char(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `genres`
--

INSERT INTO `genres` (`gen_id`, `gen_title`) VALUES
(1, 'action'),
(2, 'adventure'),
(3, 'animation'),
(4, 'biography'),
(5, 'comedy'),
(6, 'crime'),
(7, 'drama'),
(8, 'horror'),
(9, 'music'),
(10, 'mystery'),
(11, 'romance'),
(12, 'thriller'),
(13, 'war');

-- --------------------------------------------------------

--
-- Table structure for table `movie`
--

CREATE TABLE `movie` (
  `mov_id` varchar(50) NOT NULL,
  `mov_title` char(50) NOT NULL,
  `mov_year` int(11) NOT NULL,
  `mov_time` int(11) NOT NULL,
  `mov_lang` char(50) NOT NULL,
  `mov_dt_rel` date NOT NULL,
  `mov_rel_country` char(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `movie`
--

INSERT INTO `movie` (`mov_id`, `mov_title`, `mov_year`, `mov_time`, `mov_lang`, `mov_dt_rel`, `mov_rel_country`) VALUES
('10', 'Iron Man', 2009, 126, 'English', '2008-04-30', 'Indo'),
('11', 'Harry Potter and the Philosopher\'s Stone', 2001, 152, 'English', '2001-12-19', 'Indo'),
('12', 'Now You See Me', 2013, 115, 'English', '2013-05-31', 'USA'),
('13', 'Captain Marvel', 2019, 125, 'English', '2019-03-08', 'Indo'),
('15', 'Spies in Disguise', 2019, 102, 'English', '2019-12-25', 'Indo'),
('16', 'Avengers: Endgame', 2019, 181, 'English', '2019-04-24', 'Indo'),
('17', 'Toy Story 4', 2019, 100, 'English', '2019-06-21', 'Indo'),
('18', 'Spider-Man Far From Home', 2019, 129, 'English', '2019-07-03', 'Indo'),
('19', 'Onward', 2020, 102, 'English', '2020-03-04', 'Indo'),
('2', 'The Avengers', 2012, 143, 'English', '2012-05-04', 'Indo'),
('20', 'Harley Quinn: Birds of Prey', 2020, 109, 'English', '2020-02-05', 'Indo'),
('21', 'Fantastic Four', 2015, 100, 'English', '2015-08-06', 'UK'),
('22', 'Fantastic Beasts: The Secrets of Dumbledore', 2022, 142, 'English', '2022-04-13', 'Indo'),
('23', 'The Incredibles 2', 2018, 125, 'English', '2018-06-14', 'Indo'),
('24', 'Alita: Battle Angle', 2019, 122, 'English', '2019-02-05', 'Indo'),
('26', 'Kingsman: The Golden Circle', 2017, 141, 'English', '2017-09-20', 'Indo'),
('27', 'Doctor Strange in the Multiverse of Madness', 2022, 126, 'English', '2022-05-05', 'Indo'),
('28', 'Le Mans \'66', 2019, 152, 'English', '2019-11-15', 'Indo'),
('29', 'One Piece: Stampede', 2019, 101, 'Japan', '2019-09-18', 'Indo'),
('3', 'The Maze Runner', 2014, 113, 'English', '2014-09-17', 'Indo'),
('30', 'Bumblebee', 2018, 113, 'English', '2018-12-19', 'Indo'),
('4', 'Free Guy', 2021, 115, 'English', '2021-10-31', 'Indo'),
('5', 'The Amazing Spiderman', 2012, 136, 'English', '2012-07-04', 'Indo'),
('6', 'Spider-Man', 2002, 121, 'English', '2002-05-22', 'Indo'),
('62c274a9d304b', 'Top Gun: Maverick', 2022, 131, 'English', '2022-05-24', 'Indo'),
('7', 'Spider-Man Homecoming', 2017, 133, 'English', '2017-07-07', 'USA'),
('8', 'Transformers', 2007, 144, 'English', '2007-06-28', 'Indo'),
('9', 'Black Phanter', 2018, 135, 'English', '2018-02-14', 'Indo');

-- --------------------------------------------------------

--
-- Table structure for table `movie_cast`
--

CREATE TABLE `movie_cast` (
  `act_id` int(11) NOT NULL,
  `mov_id` int(11) NOT NULL,
  `role` char(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `movie_cast`
--

INSERT INTO `movie_cast` (`act_id`, `mov_id`, `role`) VALUES
(1, 1, 'Dom Toretto'),
(2, 2, 'Tony Stark'),
(3, 3, 'Thomas'),
(4, 4, 'Guy'),
(5, 5, 'Peter Parker'),
(6, 6, 'Peter Parker'),
(7, 7, 'Peter Parker'),
(8, 8, 'Sam Witwicky'),
(9, 9, 'T\'Challa'),
(2, 10, 'Tony Stark'),
(10, 11, 'Harry Potter'),
(11, 12, 'Daniel Atlas'),
(12, 13, 'Carol Danvers'),
(13, 14, 'Hobbs'),
(14, 15, 'Lance Sterling'),
(15, 16, 'Captain America'),
(16, 17, 'Woody'),
(7, 18, 'Peter Parker'),
(17, 19, 'Barley Lightfoot'),
(18, 20, 'Harley Quinn'),
(19, 21, 'Reed Ricards'),
(20, 22, 'Gellert Grindelwald'),
(21, 23, 'Frozone'),
(22, 24, 'Alita'),
(23, 25, 'Shang-Chi'),
(24, 27, 'Doctor Strange'),
(25, 28, 'Ken Miles'),
(26, 29, 'Monkey D Luffy'),
(27, 30, 'Charlie Watson'),
(28, 26, 'Eggsy');

-- --------------------------------------------------------

--
-- Table structure for table `movie_director`
--

CREATE TABLE `movie_director` (
  `dir_id` int(11) NOT NULL,
  `mov_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `movie_director`
--

INSERT INTO `movie_director` (`dir_id`, `mov_id`) VALUES
(2, 2),
(3, 3),
(4, 4),
(5, 5),
(6, 6),
(7, 7),
(8, 8),
(9, 9),
(10, 10),
(11, 11),
(12, 12),
(13, 13),
(14, 14),
(15, 15),
(16, 16),
(17, 17),
(18, 18),
(19, 19),
(20, 20),
(21, 21),
(22, 22),
(23, 23),
(24, 24),
(25, 25),
(26, 26),
(27, 27),
(28, 28),
(29, 29),
(30, 30);

-- --------------------------------------------------------

--
-- Table structure for table `movie_genres`
--

CREATE TABLE `movie_genres` (
  `mov_id` int(11) NOT NULL,
  `gen_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `movie_genres`
--

INSERT INTO `movie_genres` (`mov_id`, `gen_id`) VALUES
(1, 1),
(2, 1),
(3, 2),
(4, 5),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 2),
(12, 6),
(13, 1),
(14, 6),
(15, 3),
(16, 1),
(17, 3),
(18, 1),
(19, 3),
(20, 6),
(21, 1),
(22, 10),
(23, 3),
(24, 1),
(25, 1),
(26, 6),
(27, 2),
(28, 7),
(29, 3),
(30, 1);

-- --------------------------------------------------------

--
-- Table structure for table `rating`
--

CREATE TABLE `rating` (
  `mov_id` int(11) NOT NULL,
  `rev_id` int(11) NOT NULL,
  `rev_star` int(11) NOT NULL,
  `num_o_ratings` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `rating`
--

INSERT INTO `rating` (`mov_id`, `rev_id`, `rev_star`, `num_o_ratings`) VALUES
(1, 1, 8, 204),
(2, 2, 8, 1300000),
(3, 1, 7, 173),
(4, 2, 7, 344000),
(5, 2, 7, 643000),
(6, 1, 9, 246),
(7, 2, 7, 633000),
(8, 2, 7, 699000),
(10, 1, 9, 281),
(11, 2, 8, 790000),
(12, 2, 7, 645000),
(13, 1, 8, 544),
(14, 2, 7, 206000),
(15, 1, 8, 124),
(16, 1, 9, 550),
(17, 1, 10, 457),
(18, 1, 9, 457),
(20, 2, 6, 230000),
(21, 2, 4, 164000),
(22, 1, 5, 227),
(23, 2, 8, 287000),
(24, 1, 6, 330),
(25, 2, 7, 357000),
(26, 1, 5, 309),
(27, 2, 7, 207000),
(28, 1, 9, 357),
(29, 2, 8, 5700),
(30, 1, 9, 252);

-- --------------------------------------------------------

--
-- Table structure for table `reviewer`
--

CREATE TABLE `reviewer` (
  `rev_id` int(11) NOT NULL,
  `rev_name` char(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `reviewer`
--

INSERT INTO `reviewer` (`rev_id`, `rev_name`) VALUES
(1, 'Rotten Tomatoes'),
(2, 'IMDb'),
(3, 'Metacritic'),
(4, 'MRQE'),
(5, 'Flixster');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `actor`
--
ALTER TABLE `actor`
  ADD PRIMARY KEY (`act_id`);

--
-- Indexes for table `director`
--
ALTER TABLE `director`
  ADD PRIMARY KEY (`dir_id`);

--
-- Indexes for table `genres`
--
ALTER TABLE `genres`
  ADD PRIMARY KEY (`gen_id`);

--
-- Indexes for table `movie`
--
ALTER TABLE `movie`
  ADD PRIMARY KEY (`mov_id`);

--
-- Indexes for table `movie_director`
--
ALTER TABLE `movie_director`
  ADD KEY `director` (`dir_id`);

--
-- Indexes for table `movie_genres`
--
ALTER TABLE `movie_genres`
  ADD KEY `genre` (`gen_id`);

--
-- Indexes for table `rating`
--
ALTER TABLE `rating`
  ADD KEY `rev_id` (`rev_id`);

--
-- Indexes for table `reviewer`
--
ALTER TABLE `reviewer`
  ADD PRIMARY KEY (`rev_id`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `movie_cast`
--
ALTER TABLE `movie_cast`
  ADD CONSTRAINT `movie_cast_ibfk_2` FOREIGN KEY (`act_id`) REFERENCES `actor` (`act_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `movie_genres`
--
ALTER TABLE `movie_genres`
  ADD CONSTRAINT `movie_genres_ibfk_2` FOREIGN KEY (`gen_id`) REFERENCES `genres` (`gen_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `rating`
--
ALTER TABLE `rating`
  ADD CONSTRAINT `rating_ibfk_1` FOREIGN KEY (`rev_id`) REFERENCES `reviewer` (`rev_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
