<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Directors extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('list_director');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $this->load->library('pagination');

        $config['base_url']         = site_url('admin/directors/index');
        $config['total_rows']       = $this->db->count_all('director');;
        $config['per_page']         = 5;
        $choice                     = $config['total_rows'] / $config['per_page'];
        $config['num_links']        = floor($choice);
        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['next_link']        = 'Next';
        $config['prev_link']        = 'Prev';
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class = "page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '</span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';
        
        $this->pagination->initialize($config);
        $data['page']       = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

        $data['director']       = $this->list_director->getAll($config['per_page'], $data['page'])->result();
        $data['pagination']     = $this->pagination->create_links();
        
        $this->load->view("admin/director/list", $data);
    }

    public function add()
    {
        $director       = $this->list_director;
        $validation     = $this->form_validation;
        $validation->set_rules($director->rules());

        if ($validation->run()) {
            $director->save();
            $this->session->set_flashdata('success', 'Berhasil Disimpan');
        }

        $this->load->view("admin/director/new_form");
    }

    public function edit($id = null)
    {
        if (!isset($id)) redirect('admin/director');

        $director = $this->list_director;
        $validation = $this->form_validation;
        $validation->set_rules($director->rules());

        if ($validation->run()) {
            $director->update();
            $this->session->set_flashdata('success', 'Berhasil Disimpan');
        }

        $data["director"] = $director->getById($id);
        if (!$data["director"]) show_404();

        $this->load->view("admin/director/edit_form", $data);
    }

    public function delete($id=null)
    {
        if (!isset($id)) show_404();

        if ($this->list_director->delete($id)) {
            redirect(site_url('admin/directors'));
        }
    }

    public function search() {
        $keyword = $this->input->post('keyword');
        $data['director']=$this->list_director->get_keyword($keyword);
        $this->load->view('admin/director/search', $data);
    }
}