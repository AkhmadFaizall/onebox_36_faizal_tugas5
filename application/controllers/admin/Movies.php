<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Movies extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('list_movie');
        $this->load->library('form_validation');
    }

    public function index($id = NULL)
    {

        $this->load->library('pagination');

        $config['base_url']         = site_url('admin/movies/index');
        $config['total_rows']       = $this->db->count_all('movie');
        $config['per_page']         = 5;
        $choice                     = $config['total_rows'] / $config['per_page'];
        $config['num_links']        = floor($choice);
        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['next_link']        = 'Next';
        $config['prev_link']        = 'Prev';
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class = "page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '</span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';
        
        $this->pagination->initialize($config);
        $data['page']       = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

        $data['movie']      = $this->list_movie->getAll($config['per_page'], $data['page'])->result();
        $data['pagination'] = $this->pagination->create_links();

        $this->load->view("admin/movie/list", $data);
    }

    public function add()
    {
        $movie = $this->list_movie;
        $validation = $this->form_validation;
        $validation->set_rules($movie->rules());

        if ($validation->run()) {
            $movie->save();
            $this->session->set_flashdata('success', 'Berhasil Disimpan');
        }

        $this->load->view("admin/movie/new_form");
    }

    public function edit($id = null)
    {
        if (!isset($id)) redirect('admin/movie');

        $movie = $this->list_movie;
        $validation = $this->form_validation;
        $validation->set_rules($movie->rules());

        if ($validation->run()) {
            $movie->update();
            $this->session->set_flashdata('success', 'Berhasil Disimpan');
        }

        $data["movies"] = $movie->getById($id);
        if (!$data["movies"]) show_404();

        $this->load->view("admin/movie/edit_form", $data);
    }

    public function delete($id=null)
    {
        if (!isset($id)) show_404();

        if ($this->list_movie->delete($id)) {
            redirect(site_url('admin/movies'));
        }
    }

    public function search() {
        $keyword = $this->input->post('keyword');
        $data['movie']=$this->list_movie->get_keyword($keyword);
        $this->load->view('admin/movie/search', $data);
    }
}