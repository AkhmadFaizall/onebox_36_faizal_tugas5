<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view("admin/_partials/head.php") ?>
</head>

<body id="page-top">

    
    
    <div id="wrapper">
    
        <?php $this->load->view("admin/_partials/sidebar.php") ?>

        <div id="content-wrapper">

            <div class="content">
                <?php $this->load->view("admin/_partials/topbar.php") ?>

                <?php if ($this->session->flashdata('success')): ?>
                    <div class="alert alert-success" role="alert">
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                <?php endif; ?>

                <!-- card -->
                <div class="card mb-3">
                    
                    <div class="card-header">
                        <a href="<?php echo site_url('admin/directors') ?>"><i class="fas fa-arrow-left"></i> Back</a>
                    </div>

                    <div class="card-body">

                        <form action="" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="id" value="<?php echo $director->dir_id ?>" />
                            <div class="form-group">
                                <label for="dir_fname">Front Name*</label>
                                <input class="form-control <?php echo form_error('dir_fname') ? 'is-invalid':'' ?>" type="text" name="dir_fname" placeholder="Director Front Name" value="<?php echo $director->dir_fname ?>" />
                                <div class="invalid-feedback">
                                    <?php echo form_error('dir_fname') ?>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="dir_lname">Last Name*</label>
                                <input class="form-control <?php echo form_error('dir_lname') ? 'is-invalid':'' ?>" type="text" name="dir_lname" placeholder="Director Last Name" value="<?php echo $director->dir_lname ?>" />
                                <div class="invalid-feedback">
                                    <?php echo form_error('dir_lname') ?>
                                </div>
                            </div>

                            <input class="btn btn-success" type="submit" name="btn" value="Save" />
                        </form>

                    </div>

                    <div class="card-footer small text-muted">
                        *Required Fields
                    </div>

                </div>

            </div>
            <!-- Coutainer-fluid -->

            <!-- Sticky Footer -->
            <?php $this->load->view("admin/_partials/footer.php") ?>
        
        </div>
        <!-- Content-wrapper -->
    </div>
    <!-- Wrapper -->

    <?php $this->load->view("admin/_partials/scrolltop.php") ?>
    <?php $this->load->view("admin/_partials/js.php") ?>

</body>