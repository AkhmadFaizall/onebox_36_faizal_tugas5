<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view("admin/_partials/head.php") ?>
</head>

<body id="page-top">
    
    <div id="wrapper">
        
        <?php $this->load->view("admin/_partials/sidebar.php") ?>

        <div id="content-wrapper">

                <div class="content">
                    <?php $this->load->view("admin/_partials/topbar.php") ?>
                    
                    <!--DataTables-->
                    <div class="card mb-3">
                        <div class="card-header">
                            <a href="<?php echo site_url('admin/directors/add') ?>"><i class="fas fa-plus"></i> Add Director</a>
                        </div>
                        <br>
                        <div class="col input-group">
                            <?php echo form_open('admin/directors/search') ?>
                            <input type="text" name="keyword" class="form-control" placeholder="Search">
                                <button type="submit" class="btn btn-success">Cari</button>
                                <?php echo form_close()?>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Front Name</th>
                                            <th>Last Name</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                            <?php $no = $this->uri->segment(4); foreach ($director as $directors): ?>
                                            <tr>
                                                <td width="75"><?php echo ++$no ?></td>
                                                <td width="150">
                                                    <?php echo $directors->dir_fname ?>
                                                </td>
                                                <td>
                                                    <?php echo $directors->dir_lname ?>
                                                </td>
                                                <td width="250">
                                                    <a href="<?php echo site_url('admin/directors/edit/'. $directors->dir_id) ?>" class="btn btn-small"><i class="fas fa-edit"></i> Edit</a>
                                                    <a onclick="deleteConfirm('<?php echo site_url('admin/directors/delete/'. $directors->dir_id) ?>')" href="#!" class="btn btn-small text-danger"><i class="fas fa-trash"></i> Delete</a>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.content -->

                <!-- Sticky Footer -->
                <?php $this->load->view("admin/_partials/footer.php") ?>
            
        </div>
        <!-- /.content-wrapper -->
    
    </div>
    <!-- /.wrapper -->

    <?php $this->load->view("admin/_partials/scrolltop.php") ?>
    <?php $this->load->view("admin/_partials/modal.php") ?>
    <?php $this->load->view("admin/_partials/js.php") ?>

    <script>
        function deleteConfirm(url){
            $('#btn-delete').attr('href', url);
            $('#deleteModal').modal();
        }
    </script>

</body>
</html>