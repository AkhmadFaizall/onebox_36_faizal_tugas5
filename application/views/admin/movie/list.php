<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view("admin/_partials/head.php") ?>
</head>

<body id="page-top">
    
    <div id="wrapper">
        
        <?php $this->load->view("admin/_partials/sidebar.php") ?>

        <div id="content-wrapper">

                <div class="content">
                    <?php $this->load->view("admin/_partials/topbar.php") ?>
                    
                    <!--DataTables-->
                    <div class="card mb-3">
                        <div class="card-header">
                            <a href="<?php echo site_url('admin/movies/add') ?>"><i class="fas fa-plus"></i> Add Movie</a>
                        </div>
                        <br>
                        <div class="col input-group">
                            <?php echo form_open('admin/movies/search') ?>
                            <input type="text" name="keyword" class="form-control" placeholder="Search">
                                <button type="submit" class="btn btn-success">Cari</button>
                                <?php echo form_close()?>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Title</th>
                                            <th>Year</th>
                                            <th>Duration</th>
                                            <th>Language</th>
                                            <th>Release Date</th>
                                            <th>Release Country</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = $this->uri->segment(4); foreach ($movie as $movies): ?>
                                            <tr>
                                                <td><?php echo ++$no ?></td>
                                                <td width="150">
                                                    <?php echo $movies->mov_title ?>
                                                </td>
                                                <td>
                                                    <?php echo $movies->mov_year ?>
                                                </td>
                                                <td>
                                                    <?php echo $movies->mov_time ?>
                                                </td>
                                                <td>
                                                    <?php echo $movies->mov_lang ?>
                                                </td>
                                                <td>
                                                    <?php echo $movies->mov_dt_rel ?>
                                                </td>
                                                <td>
                                                    <?php echo $movies->mov_rel_country ?>
                                                </td>
                                                <td width="250">
                                                    <a href="<?php echo site_url('admin/movies/edit/'. $movies->mov_id) ?>" class="btn btn-small"><i class="fas fa-edit"></i> Edit</a>
                                                    <a onclick="deleteConfirm('<?php echo site_url('admin/movies/delete/'. $movies->mov_id) ?>')" href="#!" class="btn btn-small text-danger"><i class="fas fa-trash"></i> Delete</a>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <?php echo $pagination ?>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.content -->

                <!-- Sticky Footer -->
                <?php $this->load->view("admin/_partials/footer.php") ?>
            
        </div>
        <!-- /.content-wrapper -->
    
    </div>
    <!-- /.wrapper -->

    <?php $this->load->view("admin/_partials/scrolltop.php") ?>
    <?php $this->load->view("admin/_partials/modal.php") ?>
    <?php $this->load->view("admin/_partials/js.php") ?>

    <script>
        function deleteConfirm(url){
            $('#btn-delete').attr('href', url);
            $('#deleteModal').modal();
        }
    </script>

</body>
</html>