<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view("admin/_partials/head.php") ?>
</head>

<body id="page-top">
    
    <div id="wrapper">
        
        <?php $this->load->view("admin/_partials/sidebar.php") ?>

        <div id="content-wrapper">
            
            <div class="content">
                <?php $this->load->view("admin/_partials/topbar.php") ?>
                
                <?php if ($this->session->flashdata('success')): ?>
                <div class="alert alert-success" role="alert">
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php endif; ?>

                <div class="card mb-3">
                    
                    <div class="card-header">
                        <a href="<?php echo site_url('admin/movies/') ?>"><i class="fas fa-arrow-left"></i> Back</a>
                    </div>
                    
                    <div class="card-body">

                        <form action="<?php echo site_url('admin/movies/add') ?>" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="mov_title">Title*</label>
                            <input class="form-control <?php echo form_error('mov_title') ? 'is-invalid':'' ?>" type="text" name="mov_title" placeholder="Movie Title" value="<?= set_value('mov_title') ?>" />
                            <div class="invalid-feedback">
                                <?php echo form_error('mov_title') ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="mov_year">Year*</label>
                            <input class="form-control <?php echo form_error('mov_year') ? 'is-invalid':'' ?>" type="number" name="mov_year" placeholder="Year Release" value="<?= set_value('mov_year') ?>" />
                            <div class="invalid-feedback">
                                <?php echo form_error('mov_yaer') ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="mov_time">Duration*</label>
                            <input class="form-control <?php echo form_error('mov_time') ? 'is-invalid':'' ?>" type="number" name="mov_time" placeholder="Movie Duration (in minute)" value="<?= set_value('mov_time') ?>" />
                            <div class="invalid-feedback">
                                <?php echo form_error('mov_time') ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="mov_lang">Language*</label>
                            <input class="form-control <?php echo form_error('mov_lang') ? 'is-invalid':'' ?>" type="text" name="mov_lang" placeholder="Movie Language" value="<?= set_value('mov_lang') ?>" />
                            <div class="invalid-feedback">
                                <?php echo form_error('mov_lang') ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="mov_dt_rel">Release Date*</label>
                            <input class="form-control <?php echo form_error('mov_dt_rel') ? 'is-invalid':'' ?>" type="date" name="mov_dt_rel" placeholder="Release Date" value="<?= set_value('mov_dt_rel') ?>" />
                            <div class="invalid-feedback">
                                <?php echo form_error('mov_dt_rel') ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="mov_rel_country">Release Country*</label>
                            <input class="form-control <?php echo form_error('mov_rel_country') ? 'is-invalid':'' ?>" type="text" name="mov_rel_country" placeholder="Release In" value="<?= set_value('mov_rel_country') ?>" />
                            <div class="invalid-feedback">
                                <?php echo form_error('mov_rel_country') ?>
                            </div>
                        </div>

                        <input class="btn btn-success" type="submit" name="btn" value="Save" />
                        </form>
                    </div>

                    <div class="card-footer small text-muted">
                        *Required Fields
                    </div>
                
                </div>
            
            </div>
            <!-- Content -->

            <!-- Sticky Footer -->
            <?php $this->load->view("admin/_partials/footer.php") ?>

        </div>
        <!-- Content-wrapper -->

    </div>
    <!-- Wrapper -->

    <?php $this->load->view("admin/_partials/scrolltop.php") ?>
    <?php $this->load->view("admin/_partials/js.php") ?>

</body>