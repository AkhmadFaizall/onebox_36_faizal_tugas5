<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view("admin/_partials/head.php") ?>
</head>

<body id="page-top">
    
    <div id="wrapper">
    
        <?php $this->load->view("admin/_partials/sidebar.php") ?>

        <div id="content-wrapper">

            <div class="content">
                <?php $this->load->view("admin/_partials/topbar.php") ?>

                <!-- card -->
                <div class="card mb-3">
                    <div class="card-body">
                        <form
                            class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
                            <div class="input-group">
                                <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..."
                                    aria-label="Search" aria-describedby="basic-addon2">
                                <div class="input-group-append">
                                    <button class="btn btn-light" type="button">
                                        <i class="fas fa-search fa-sm"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-4 col-xs-offset-4 text-center">
                        <h3>Data Movie</h3>
                        <p style="color:orange"><b>Menampilkan data dengan kata kunci : "<?= $keyword; ?></b>></p>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">No.</th>
                                    <th scope="col">Title</th>
                                    <th scope="col">Year</th>
                                    <th scope="col">Duration</th>
                                    <th scope="col">Language</th>
                                    <th scope="col">Release Date</th>
                                    <th scope="col">Release Country</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1; foreach ($movie as $movies): ?>
                                    <tr>
                                        <td><?php echo $no++ ?></td>
                                        <td width="150">
                                            <?php echo $movies->mov_title ?>
                                        </td>
                                        <td>
                                            <?php echo $movies->mov_year ?>
                                        </td>
                                        <td>
                                            <?php echo $movies->mov_time ?>
                                        </td>
                                        <td>
                                            <?php echo $movies->mov_lang ?>
                                        </td>
                                        <td>
                                            <?php echo $movies->mov_dt_rel ?>
                                        </td>
                                        <td>
                                            <?php echo $movies->mov_rel_country ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- Coutainer-fluid -->

            <!-- Sticky Footer -->
            <?php $this->load->view("admin/_partials/footer.php") ?>
        
        </div>
        <!-- Content-wrapper -->
    </div>
    <!-- Wrapper -->

    <?php $this->load->view("admin/_partials/scrolltop.php") ?>
    <?php $this->load->view("admin/_partials/js.php") ?>

</body>

