<?php defined('BASEPATH') OR exit('No direct script access allowed');

class List_director extends CI_Model {
    private $_table = "director";

    public $dir_id;
    public $dir_fname;
    public $dir_lname;

    public function rules(){
        return [
            ['field' => 'dir_fname',
            'label' => 'Front Name',
            'rules' => 'required'],

            ['field' => 'dir_lname',
            'label' => 'Last Name',
            'rules' => 'required'],
        ];
    }

    public function getAll($limit, $offset)
    {
        $query = $this->db->get($this->_table, $limit, $offset);
        return $query;
    }

    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["dir_id" => $id])->row();
    }

    public function save()
    {
        $post                   = $this->input->post();
        $this->dir_id           = uniqid();
        $this->dir_fname        = $post["dir_fname"];
        $this->dir_lname        = $post["dir_lname"];
        return $this->db->insert($this->_table, $this);
    }

    public function update()
    {
        $post                   = $this->input->post();
        $this->dir_id           = $post['id'];
        $this->dir_fname        = $post["dir_fname"];
        $this->dir_lname        = $post["dir_lname"];
        return $this->db->update($this->_table, $this, array('dir_id' =>$post['id']));
    }

    public function delete($id)
    {
        return $this->db->delete($this->_table, array("dir_id" => $id));
    }

    public function get_keyword($keyword) {
        $this->db->select('*');
        $this->db->from('director');
        $this->db->like('dir_fname', $keyword);
        $this->db->not_like('dir_lname', $keyword);
        return $this->db->get()->result();
    }
}