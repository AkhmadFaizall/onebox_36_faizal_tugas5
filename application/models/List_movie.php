<?php defined('BASEPATH') OR exit('No direct script access allowed');

class List_movie extends CI_Model {
    private $_table = "movie";

    public $mov_id;
    public $mov_title;
    public $mov_year;
    public $mov_time;
    public $mov_lang;
    public $mov_dt_rel;
    public $mov_rel_country;

    public function rules(){
        return [
            ['field' => 'mov_title',
            'label' => 'Title',
            'rules' => 'required'],

            ['field' => 'mov_year',
            'label' => 'Year',
            'rules' => 'required'],

            ['field' => 'mov_time',
            'label' => 'Duration',
            'rules' => 'required'],

            ['field' => 'mov_lang',
            'label' => 'Language',
            'rules' => 'required'],

            ['field' => 'mov_dt_rel',
            'label' => 'Release Date',
            'rules' => 'required'],

            ['field' => 'mov_rel_country',
            'label' => 'Release Country',
            'rules' => 'required'],
        ];
    }

    public function getAll($limit, $offset)
    {
        $query = $this->db->get($this->_table, $limit, $offset);
        return $query;
    }

    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["mov_id" => $id])->row();
    }

    public function save()
    {
        $post                   = $this->input->post();
        $this->mov_title        = $post["mov_title"];
        $this->mov_year         = $post["mov_year"];
        $this->mov_time         = $post["mov_time"];
        $this->mov_lang         = $post["mov_lang"];
        $this->mov_dt_rel       = $post["mov_dt_rel"];
        $this->mov_rel_country  = $post["mov_rel_country"];
        return $this->db->insert($this->_table, $this);
    }

    public function update()
    {
        $post                   = $this->input->post();
        $this->mov_id           = $post['id'];
        $this->mov_title        = $post["mov_title"];
        $this->mov_year         = $post["mov_year"];
        $this->mov_time         = $post["mov_time"];
        $this->mov_lang         = $post["mov_lang"];
        $this->mov_dt_rel       = $post["mov_dt_rel"];
        $this->mov_rel_country  = $post["mov_rel_country"];
        return $this->db->update($this->_table, $this, array('mov_id' =>$post['id']));
    }

    public function delete($id)
    {
        return $this->db->delete($this->_table, array("mov_id" => $id));
    }

    public function get_keyword($keyword) {
        $this->db->select('*');
        $this->db->from('movie');
        $this->db->like('mov_title', $keyword);
        $this->db->not_like('mov_year', $keyword);
        return $this->db->get()->result();
    }

}